#!/bin/bash

function get_script_dir()
{
    local DIR=""
    local SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
        DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    THIS_SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
}
get_script_dir

function clear_dependencies()
{
    PACKS=$(node scripts/wipe-dependencies.js)
}

function do_update()
{
    PACKAGES=$(npm outdated | tail -n +2 | cut -d" " -f 1)
    for P in $PACKAGES; do
        npm update P
        if [ "$?" != "0" ]; then
            echo "Error during updating of packages"
            exit 1
        fi
    done
}

function save_packages()
{
    local DEV=1
    for P in $PACKS; do
        if [ "$DEV" = "1" ]; then
            if [ "$P" = "----" ]; then
                DEV=0
            else
                npm install --save-dev $P
                if [ "$?" != "0" ]; then
                    echo "Error during update of dev deps"
                    exit 1
                fi
            fi
        else
            npm install --save $P
            if [ "$?" != "0" ]; then
                echo "Error during update of prod deps"
                exit 1
            fi
        fi
    done
}

cd $THIS_SCRIPT_DIR/..
if [ "$1" = "--only" ]; then
    do_update
else
    # cp package.json package.bck.json
    clear_dependencies
    do_update
    save_packages
fi
echo "Done :)"

# EOF
