import {
    promisify as oroginalPromisify
} from "util";


/**
 * @brief Creates a promise-version of the input function.
 * The input function must satisfy the standard promisify() method.
 * Internally uses the standard promisify() method.
 * @typeParam InType The input type of the method.
 * @typeParam RestType The types for the options.
 * @typeParam ResType The type of the result.
 * @param {(
 *     ...args: [
 *         InType,
 *         ...RestType,
 *         (err: Error | null, res: ResType) => void
 *     ]
 * ) => void} foo The input function.
 * @param {...RestType} rest All other parameters to be passed to the function,
 *     but the first.
 */
function promisify<
    InType extends unknown,
    RestType extends [unknown],
    ResType extends unknown
>(
    foo: (
        ...args: [
            InType,
            ...RestType,
            (err: Error | null, res: ResType) => void
        ]
    ) => void,
    ...rest: RestType
): (param1: InType) => Promise<ResType>
{
    function ret(param: InType): Promise<ResType>
    {
        const promiseFoo = oroginalPromisify(foo);
        return promiseFoo(param, ...rest);
    }

    return ret;
}


export {
    promisify
};
