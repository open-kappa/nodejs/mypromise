import {
    foreachPromise
} from "./index";
import fs from "fs";
import path from "path";


/**
 * @brief Check for file existence.
 * @param {string} file The file.
 * @return {Promise<boolean>} True if the file exists.
 */
function fileExists(file: string): Promise<boolean>
{
    function executor(
        resolve: (value: boolean | PromiseLike<boolean>) => void,
        _reject: (reason?: any) => void
    ): void
    {
        function onCheck(err: NodeJS.ErrnoException | null): void
        {
            const res = err === null;
            resolve(res);
        }
        fs.access(file, fs.constants.R_OK, onCheck);
    }

    return new Promise<boolean>(executor);
}


/**
 * @brief List all files in a direcroty.
 * @param {string} extension The optional filter on file extension.
 * @param {boolean} recursive Whether to recurse into directories. Default is
 *     false.
 * @return {(dirname: string) => Promise<Array<string>>} The list of files
 *     function.
 */
function listFiles(
    extension = "",
    recursive = false
): (dirname: string) => Promise<Array<string>>
{
    type FilterResult = {
        isDir: boolean,
        isFile: boolean,
        path: string
    };

    function doFilter(file: string, stats: fs.Stats): Promise<FilterResult>
    {
        const ret: FilterResult = {
            "isDir": stats.isDirectory(),
            "isFile": stats.isFile() && file.endsWith(extension),
            "path": file
        };
        return Promise.resolve(ret);
    }

    function recall(
        filterRes: FilterResult
    ): Promise<Array<string>>
    {
        const ret = new Array<string>();
        if (filterRes.isFile)
        {
            ret.push(filterRes.path);
            return Promise.resolve(ret);
        }
        else if (!recursive || !filterRes.isDir)
        {
            return Promise.resolve(ret);
        }
        function recallWrapper(
            what: Array<string>
        ): Promise<Array<Array<string>>>
        {
            const ls = listFiles(extension, recursive);
            const forLs = foreachPromise(ls);
            function doJoin(ww: string): string
            {
                return path.join(filterRes.path, ww);
            }
            const list = what.map(doJoin);
            return forLs(list);
        }
        function doAppend(
            lsResult: Array<Array<string>>
        ): Promise<Array<string>>
        {
            for (const arr of lsResult.values())
            {
                Array.prototype.push.apply(ret, arr);
            }
            return Promise.resolve(ret);
        }
        return fs.promises.readdir(filterRes.path)
            .then(recallWrapper)
            .then(doAppend);
    }

    function retFoo(dirname: string): Promise<Array<string>>
    {
        function doFilterWrapper(
            stat: fs.Stats
        ): Promise<FilterResult>
        {
            return doFilter(dirname, stat);
        }
        return fs.promises.stat(dirname)
            .then(doFilterWrapper)
            .then(recall);
    }

    return retFoo;
}


export {
    fileExists,
    listFiles
};
