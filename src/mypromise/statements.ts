/**
 * @brief Return a method which execute a callback based on a boolean.
 * @typeParam RetType The returned type.
 * @param {() => Promise<RetType>} thenFoo The callback of the true branch.
 * @param {() => Promise<RetType>} elseFoo The callback of the false branch.
 * @return (cc: boolean) => Promise<RetType> The wrapper function.
 */
function ifBoolPromise<RetType>(
    thenFoo: () => Promise<RetType>,
    elseFoo: () => Promise<RetType>
): (cc: boolean) => Promise<RetType>
{
    function ret(cond: boolean): Promise<RetType>
    {
        if (cond) return thenFoo();
        return elseFoo();
    }
    return ret;
}


/**
 * @brief Return a method which execute a callback based on a boolean.
 * @typeParam InData The input data type.
 * @typeParam RetType The returned type.
 * @param {() => Promise<RetType>} thenFoo The callback of the true branch.
 * @param {() => Promise<RetType>} elseFoo The callback of the false branch.
 * @return (cc: boolean) => Promise<RetType> The wrapper function.
 */
function ifPromise<InData, RetType>(
    condFoo: (inData: InData) => Promise<boolean>,
    thenFoo: (inData: InData) => Promise<RetType>,
    elseFoo: (inData: InData) => Promise<RetType>
): (inData: InData) => Promise<RetType>
{
    function ret(inData: InData): Promise<RetType>
    {
        function thenWrapper(): Promise<RetType>
        {
            return thenFoo(inData);
        }
        function elseWrapper(): Promise<RetType>
        {
            return elseFoo(inData);
        }
        const ifCond = ifBoolPromise(thenWrapper, elseWrapper);

        return condFoo(inData).then(ifCond);
    }
    return ret;
}


/**
 * @brief Return an iterator over an array of data.
 * @typeParam InType The type fo the input.
 * @typeParam RetType The returned element type.
 * @param {(inData: InType) => Promise<RetType>} foo The callback for each
 *     promise.
 * @return {(inData: Array<InType>) => Promise<Array<RetType>>} The wrappping
 *     method.
 */
function foreachPromise<InType, RetType>(
    foo: (inData: InType) => Promise<RetType>
): (inData: Array<InType>) => Promise<Array<RetType>>
{
    function ret(input: Array<InType>): Promise<Array<RetType>>
    {
        const retArray: Array<RetType> = [];
        if (input.length === 0) return Promise.resolve(retArray);
        let i = 0;

        function doCollect(el: RetType): Promise<Array<RetType>>
        {
            retArray.push(el);
            i += 1;
            if (i >= input.length) return Promise.resolve(retArray);
            return foo(input[i]).then(doCollect);
        }

        return foo(input[i]).then(doCollect);
    }
    return ret;
}


/**
 * @brief Return a parallel iterator over an array of data.
 * @typeParam InType The type fo the input.
 * @typeParam RetType The returned element type.
 * @param {(inData: InType) => Promise<RetType>} foo The callback for each
 *     promise.
 * @return {(inData: Array<InType>) => Promise<Array<RetType>>} The wrappping
 *     method.
 */
function parForeachPromise<InType, RetType>(
    foo: (inData: InType) => Promise<RetType>
): (inData: Array<InType>) => Promise<Array<RetType>>
{
    function ret(input: Array<InType>): Promise<Array<RetType>>
    {
        const retArray: Array<RetType> = [];
        if (input.length === 0) return Promise.resolve(retArray);
        const anyRet: Array<any> = retArray;
        const allPromises: Array<Promise<void>> = [];
        for (let i = 0; i < input.length; ++i)
        {
            anyRet.push(null);
            // eslint-disable-next-line no-inner-declarations
            function doCollect(value: RetType): Promise<void>
            {
                retArray[i] = value;
                return Promise.resolve();
            }
            allPromises.push(
                Promise.resolve(input[i])
                    .then(foo)
                    .then(doCollect)
            );
        }

        function doReturn(): Promise<Array<RetType>>
        {
            return Promise.resolve(retArray);
        }

        return Promise.all(allPromises).then(doReturn);
    }
    return ret;
}


/**
 * @brief Return a function which always rejects.
 * @typeParam RetType The promise return type.
 * @param {string} msg The error message.
 * @return {() => Promise<RetType>} The method.
 */
function raisePromiseError<RetType>(
    msg: string
): () => Promise<RetType>
{
    function ret(): Promise<RetType>
    {
        return Promise.reject(new Error(msg));
    }
    return ret;
}


/**
 * @brief Resolve a promise.
 * Useful to avoid typing issues in case of promises returning non void.
 * @return {Promise<void>} A resolved promise.
 */
function toVoid(): Promise<void>
{
    return Promise.resolve();
}

/**
 * @brief Return an loop promise.
 * @typeParam InType The type of the input.
 * @typeParam RetType The type of the output.
 * @param {(inData: InType) => Promise<boolean>} condition The callback to
 *     evaluate the condition.
 * @param {(inData) => Promise<RetType>} body The loop body.
 *     method.
 * @return {(inData: InType) => Promise<Array<RetType>>} The loop
 *     implementation.
 */
function whilePromise<InType, RetType>(
    condition: (inData: InType) => Promise<boolean>,
    body: (inData: InType) => Promise<RetType>
): (inData: InType) => Promise<Array<RetType>>
{
    const retArray: Array<RetType> = [];

    function returnTrue(val: RetType): Promise<boolean>
    {
        retArray.push(val);
        return Promise.resolve(true);
    }
    function thenBody(input: InType): Promise<boolean>
    {
        return body(input).then(returnTrue);
    }
    function emptyElse(_skip: InType): Promise<boolean>
    {
        return Promise.resolve(false);
    }

    function elseNone(): Promise<Array<RetType>>
    {
        return Promise.resolve(retArray);
    }

    // eslint-disable-next-line no-use-before-define
    const ifBody = ifPromise(condition, thenBody, emptyElse);

    function ret(input: InType): Promise<Array<RetType>>
    {
        function doLoop(): Promise<Array<RetType>>
        {
            // eslint-disable-next-line no-use-before-define
            const ifCondition = ifBoolPromise(doLoop, elseNone);
            return ifBody(input).then(ifCondition);
        }
        return doLoop();
    }

    return ret;
}


export {
    foreachPromise,
    ifBoolPromise,
    ifPromise,
    parForeachPromise,
    raisePromiseError,
    toVoid,
    whilePromise
};
