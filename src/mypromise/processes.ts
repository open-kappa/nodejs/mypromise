import childProcess from "child_process";


/**
 * @brief Interface for exec and spawn options.
 */
export interface ExecOptions
{

    /**
     * @brief The callback to trace the stdout. Default null.
     */
    logStdout?: ((data: string) => void) | null;

    /**
     * @brief The callback to trace the stderr. Default null.
     */
    logStderr?: ((data: string) => void) | null;

    /**
     * @brief The callback to trace the return code. Only considered for spawn.
     * Default null.
     */
    logCode?: ((data: number) => void) | null;

    /**
     * @brief Whether to consider a non-zero return code as an error.
     * Only considered for spawn. Default true.
     */
    checkCode?: boolean;

    /**
     * @brief Whether to prepend the stderr to the possible error object
     * message. Only considered if logStderr is null and for exec.
     * Default false.
     */
    prependStderr?: boolean;

    /**
     * @brief Whether to prepend the stderr to the possible error object
     * message. Only considered if logStdout is null and for exec.
     * Default false.
     */
    prependStdout?: boolean;
}


/**
 * @brief Return a promise method to execute a command.
 * Internally call childProcess.exec().
 * @param {ExecOptions} options The exec options.
 * @return {(cmd: Array<string>) => Promise<void>} The callback
 *     accepting the command and returning the child process handle.
 */
function exec(
    options: ExecOptions = {}
): (cmd: Array<string>) => Promise<void>
{
    function ret(cmd: Array<string>): Promise<void>
    {
        function executor(
            resolve: () => void,
            reject: (reason?: any) => void
        ): void
        {
            function doCheck(
                err: Error | null,
                stdout: string,
                stderr: string
            ): void
            {
                if (options.logStdout) options.logStdout(stdout);
                if (options.logStderr) options.logStderr(stderr);
                if (err !== null)
                {
                    let msg = err.message;
                    if (!options.logStdout && options.prependStdout)
                    {
                        msg = stdout + "\n" + msg;
                    }
                    if (!options.logStderr && options.prependStderr)
                    {
                        msg = stderr + "\n" + msg;
                    }
                    err.message = msg;
                    reject(err);
                    return;
                }

                resolve();
            }
            childProcess.exec(cmd.join(" "), doCheck);
        }
        return new Promise<void>(executor);
    }
    return ret;
}

/**
 * @brief Return a promise method to execute a command.
 * Internally call childProcess.spawn().
 * @param {ExecOptions} options The exec options.
 * @return {(cmd: string) => Promise<void>} The callback
 *     accepting the command and returning the child process handle.
 */
function spawn(
    options: ExecOptions = {}
): (cmd: Array<string>) => Promise<void>
{
    function ret(cmd: Array<string>): Promise<void>
    {
        function executor(
            resolve: () => void,
            reject: (reason?: any) => void
        ): void
        {
            function onComplete(code: number): void
            {
                if (options.logCode) options.logCode(code);
                if ((typeof options.checkCode === "undefined"
                    || options.checkCode)
                    && code !== 0)
                {
                    reject(new Error(String(code)));
                    return;
                }
                resolve();
            }
            function onError(error: Error): void
            {
                reject(error);
            }

            const cmdString = cmd[0];
            const args = cmd.slice(1);
            const cmdObj = childProcess.spawn(cmdString, args);
            if (options.logStdout) cmdObj.stdout.on("data", options.logStdout);
            if (options.logStderr) cmdObj.stderr.on("data", options.logStderr);
            cmdObj.on("error", onError);
            cmdObj.on("close", onComplete);
        }
        return new Promise<void>(executor);
    }
    return ret;
}


/**
 * @brief Wraps exec or spawn to collect their stdout as a string.
 * @param {(options: ExecOptions) => ((cmd: Array<string>) => Promise<void>)}foo
 *     the function to wrap.
 * @param {ExecOptions} options The options.
 */
function collectStdout(
    foo: (opts: ExecOptions) => ((cmd: Array<string>) => Promise<void>),
    options: ExecOptions = {}
): (cmd: Array<string>) => Promise<string>
{
    let output = "";
    function mylog(msg: string): Promise<void>
    {
        output += msg;
        return Promise.resolve();
    }
    const originalLog = options.logStdout;
    options.logStdout = mylog;

    const originalRet = foo(options);

    function returnString(): Promise<string>
    {
        if (originalLog) originalLog(output);
        return Promise.resolve(output);
    }

    function myRet(cmd: Array<string>): Promise<string>
    {
        return originalRet(cmd).then(returnString);
    }

    return myRet;
}

export {
    collectStdout,
    exec,
    spawn
};
