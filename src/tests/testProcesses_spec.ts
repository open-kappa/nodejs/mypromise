/* eslint-disable class-methods-use-this */
import {
    collectStdout,
    exec,
    spawn
} from "../mypromise/index";
import {MyTest} from "@open-kappa/mytest";

class ThisTest
    extends MyTest
{
    constructor()
    {
        super("test processes");
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerPromiseTest("test-exec", self.testExec);
        self.registerPromiseTest("test-spawn", self.testSpawn);
    }

    private testExec(): Promise<void>
    {
        function doCheck(res: string): Promise<void>
        {
            if (res.indexOf("package.json") !== -1) return Promise.resolve();
            return Promise.reject(
                new Error("Wrong execution")
            );
        }
        const cmd = ["ls", "."];
        const foo = collectStdout(exec);
        return foo(cmd).then(doCheck);
    }

    private testSpawn(): Promise<void>
    {
        function doCheck(res: string): Promise<void>
        {
            if (res.indexOf("package.json") !== -1) return Promise.resolve();
            return Promise.reject(
                new Error("Wrong execution")
            );
        }
        const cmd = ["ls", "."];
        const foo = collectStdout(spawn);
        return foo(cmd).then(doCheck);
    }
}

const test = new ThisTest();
test.run();
