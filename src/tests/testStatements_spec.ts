/* eslint-disable class-methods-use-this */
import {
    foreachPromise,
    ifBoolPromise,
    ifPromise,
    parForeachPromise,
    raisePromiseError,
    toVoid,
    whilePromise
} from "../mypromise/index";
import {MyTest} from "@open-kappa/mytest";

class ThisTest
    extends MyTest
{
    constructor()
    {
        super("test statements");
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerPromiseTest("test-if-bool", self.testIfBool);
        self.registerPromiseTest("test-if", self.testIf);
        self.registerPromiseTest("test-foreach", self.testForeach);
        self.registerPromiseTest("test-parforeach", self.testParForeach);
        self.registerPromiseTest("test-raise", self.testRaise, true);
        self.registerPromiseTest("test-void", self.testVoid);
        self.registerPromiseTest("test-while", self.testWhile);
    }

    private testIfBool(): Promise<void>
    {
        function thenFoo(): Promise<number>
        {
            return Promise.resolve(1);
        }

        function elseFoo(): Promise<number>
        {
            return Promise.resolve(0);
        }

        function doCheck(num: number): Promise<void>
        {
            if (num === 0) return Promise.resolve();
            return Promise.reject(
                new Error("Wrong execution")
            );
        }

        const ifStatement = ifBoolPromise(thenFoo, elseFoo);
        return ifStatement(false).then(doCheck);
    }

    private testIf(): Promise<void>
    {
        function condFoo(name: string): Promise<boolean>
        {
            return Promise.resolve(name === "Pippo");
        }

        function thenFoo(name: string): Promise<number>
        {
            if (name === "Pippo") return Promise.resolve(1);
            return Promise.reject(
                new Error("Wrong name")
            );
        }

        function elseFoo(name: string): Promise<number>
        {
            if (name === "Pippo") return Promise.resolve(0);
            return Promise.reject(
                new Error("Wrong name")
            );
        }

        function doCheck(num: number): Promise<void>
        {
            if (num === 1) return Promise.resolve();
            return Promise.reject(
                new Error("Wrong execution")
            );
        }

        const ifStatement = ifPromise(condFoo, thenFoo, elseFoo);
        return ifStatement("Pippo").then(doCheck);
    }

    private testForeach(): Promise<void>
    {
        function foo(num: number): Promise<number>
        {
            return Promise.resolve(-num);
        }

        function doCheck(num: Array<number>): Promise<void>
        {
            if (num.length !== 10)
            {
                return Promise.reject(
                    new Error("Wrong result length")
                );
            }
            for (let i = 0; i < 10; ++i)
            {
                if (num[i] === -i) continue;
                return Promise.reject(
                    new Error("Wrong num result")
                );
            }
            return Promise.resolve();
        }

        const input = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        const forStatement = foreachPromise(foo);
        return forStatement(input).then(doCheck);
    }

    private testParForeach(): Promise<void>
    {
        function foo(num: number): Promise<number>
        {
            return Promise.resolve(-num);
        }

        function doCheck(num: Array<number>): Promise<void>
        {
            if (num.length !== 10)
            {
                return Promise.reject(
                    new Error("Wrong result length")
                );
            }
            for (let i = 0; i < 10; ++i)
            {
                if (num[i] === -i) continue;
                return Promise.reject(
                    new Error("Wrong num result")
                );
            }
            return Promise.resolve();
        }

        const input = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        const parForStatement = parForeachPromise(foo);
        return parForStatement(input).then(doCheck);
    }

    private testRaise(): Promise<void>
    {
        const foo = raisePromiseError<void>("Oh no!");
        return foo();
    }

    private testVoid(): Promise<void>
    {
        return Promise.resolve(4).then(toVoid);
    }

    private testWhile(): Promise<void>
    {
        let index = 0;
        function cond(num: Array<number>): Promise<boolean>
        {
            return Promise.resolve(index < num.length);
        }
        function body(num: Array<number>): Promise<number>
        {
            const nn = -num[index];
            index += 1;
            return Promise.resolve(nn);
        }
        function doCheck(num: Array<number>): Promise<void>
        {
            if (num.length !== 10)
            {
                return Promise.reject(
                    new Error("Wrong result length")
                );
            }
            for (let i = 0; i < 10; ++i)
            {
                if (num[i] === -i) continue;
                return Promise.reject(
                    new Error("Wrong num result")
                );
            }
            return Promise.resolve();
        }

        const input = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        const whileStatement = whilePromise(cond, body);
        return whileStatement(input).then(doCheck);
    }
}

const test = new ThisTest();
test.run();
