/* eslint-disable class-methods-use-this */
import {MyTest} from "@open-kappa/mytest";
import {
    promisify
} from "../mypromise/index";


class ThisTest
    extends MyTest
{
    constructor()
    {
        super("test generic");
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerPromiseTest("test-promisify", self.testPromisify);
    }

    private testPromisify(): Promise<void>
    {
        function doCheck(res: boolean): Promise<void>
        {
            if (res) return Promise.resolve();
            return Promise.reject(
                new Error("Wrong execution")
            );
        }

        function testFoo(
            num: number,
            check: number,
            cb: (err: Error | null, res: boolean) => void
        ): void
        {
            const res = num !== check;
            cb(null, res);
        }

        const foo = promisify(testFoo, 10);

        return foo(20).then(doCheck);
    }
}

const test = new ThisTest();
test.run();
