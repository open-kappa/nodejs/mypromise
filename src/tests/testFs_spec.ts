/* eslint-disable class-methods-use-this */
import {
    fileExists,
    listFiles
} from "../mypromise/index";
import {MyTest} from "@open-kappa/mytest";

class ThisTest
    extends MyTest
{
    constructor()
    {
        super("test fs");
    }

    protected registerTests(): void
    {
        const self = this;
        self.registerPromiseTest("test-fileExists", self.testFileExists);
        self.registerPromiseTest("test-listFiles", self.testListFiles);
    }

    private testFileExists(): Promise<void>
    {
        function doCheck(res: boolean): Promise<void>
        {
            if (res) return Promise.resolve();
            return Promise.reject(
                new Error("Wrong execution")
            );
        }

        return fileExists(__filename).then(doCheck);
    }

    private testListFiles(): Promise<void>
    {
        const expectedFiles = [
            "dist/tests/testFs_spec.js",
            "dist/tests/testGeneric_spec.js",
            "dist/tests/testProcesses_spec.js",
            "dist/tests/testStatements_spec.js"
        ];
        function doCheck(res: Array<string>): Promise<void>
        {
            if (res.length !== 4)
            {
                return Promise.reject(
                    new Error(
                        "Wrong result length:" + res.length
                        + ", res:" + JSON.stringify(res)
                    )
                );
            }
            for (const file of res.values())
            {
                if (expectedFiles.indexOf(file) !== -1) continue;
                return Promise.reject(
                    new Error("Wrong execution: " + file)
                );
            }
            return Promise.resolve();
        }
        const ls = listFiles(".js", true);
        return ls("./dist/tests").then(doCheck);
    }
}

const test = new ThisTest();
test.run();
