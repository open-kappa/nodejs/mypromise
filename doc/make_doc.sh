#!/bin/bash

function favicon2()
{
    for file in $(ls public/*.html); do
    sed -i 's/<\/head>/<link rel="icon" type="image\/ico" href="favicon.ico"><\/head>/g' $file
    done
    for file in $(ls public/*/*.html); do
        sed -i 's/<\/head>/<link rel="icon" type="image\/ico" href="..\/favicon.ico"><\/head>/g' $file
    done
    for file in $(ls public/*/*/*.html); do
        sed -i 's/<\/head>/<link rel="icon" type="image\/ico" href="..\/..\/favicon.ico"><\/head>/g' $file
    done

    cp doc/favicon.ico doc/mypromise.png public
}

if [ "1" = "0" ]; then
    node_modules/.bin/jsdoc \
        -c doc/docdash.json
    favicon2
else
    node_modules/.bin/typedoc \
        --options doc/typedoc.json
    favicon2
fi

echo "Doc creation completed."

# EOF
