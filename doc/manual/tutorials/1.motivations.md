This simple package provides some utilities to write promise-based code.

The main features are:
1. Provide easy chaining of promises.
1. Easying the  writing of complex data flows.

Both are sketched in the follwing.

## Promise chaining

Many API methods are just not so easy to put into a promise chain. For example
this is one of the TypeScript signaures for the standard `spawn()` method:

```typescript
function spawn(
    command: string,
    args?: ReadonlyArray<string>,
    options?: SpawnOptionsWithoutStdio
): ChildProcessWithoutNullStreams;
```

This is the disadvantage: it is not so easy to chain it in a `.then()` sequence,
since it takes many arguments, and returns no promise.
The idea is to implement a simple wrapper, which provides one more level of
indirection, to return a promise and eliminate the configuration parameters.
For example, something like:

```typescript
function doWrapper(
    args?: ReadonlyArray<string>,
    options?: SpawnOptionsWithoutStdio
): (command: string) => Promise<ChildProcessWithoutNullStreams>
```

By separating the moments of "method configuration" and "method execution",
we can easyly chain it, for example:

```typescript
function getProcessCommand(): Promise<string>;

getProcessCommand().then(doWrapper);
```

## Writing of complex data flows

Promise chainng is usually quite linear.
This linearity does not fits well when compared to normal data
manipulation:
1. Conditional paths are missing (`if`).
1. Loops on data structures are missing as well (`for`).

This library provides some statements to cover these cases.
