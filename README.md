# @open-kappa/mypromise
This package provides a simple library to simplify promise-based code.

# Links

 * Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/mypromise)

 * Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/mypromise)

 * List of open-kappa projects, [hosted on GitLab Pages](
   https://open-kappa.gitlab.io)

# License

*@open-kappa/mypromise* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
